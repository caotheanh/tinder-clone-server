##Install
npm install

##Create database
npx sequelize-cli db:create

##Migration
npx sequelize-cli db:migrate

##Seed data user
npx sequelize-cli db:seed:all