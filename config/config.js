require("dotenv").config();
module.exports = {
  username: "root" || process.env.USERNAME,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  host: process.env.HOST,
  dialect: "mysql",
};
